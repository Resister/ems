# Core EMS ⚡

## Overview

This monorepo contains the core api and web ui for the EMS product. It uses the **MERN** stack with typescript and
influxdb. It also uses Yarn Workspaces for monorepo management and **trunk based development** with **rebasing**.

- The api has been designed, so it can eventually be turned into gateway for other specialized microservices in their
  own repos. Following an [API Gateway pattern](https://microservices.io/patterns/apigateway.html).
- The web ui has been designed, so it can be added as a package in this monorepo as a React Native mobile app.

The api and web ui also interface with kubernetes services, like so:

```plantuml
!include https://gitlab.com/-/snippets/2358642/raw/main/style.puml
rectangle web #line.bold {
    rectangle material_ui
    rectangle mqtt_react_hooks
    rectangle react_query
    material_ui -- mqtt_react_hooks
    material_ui -- react_query
}

rectangle kubernetes #line.dashed{
    database mongodb
    database influxdb
    rectangle mosquitto
    rectangle telegraf
    rectangle api #line.bold
    rectangle keycloak
}

collections controllers

react_query -- api
mosquitto -- telegraf
telegraf -right- influxdb
mqtt_react_hooks -- mosquitto
influxdb -up- api
api -- mongodb
api -right- keycloak
react_query -- keycloak
mosquitto -left- controllers
```

> **Note:** The boxes with the thicker lines correspond to the code in the web and api directories in this repo. The
> other boxes are third-party services. Third party services are managed in the [platform repository]().

This repo is organized in order to empower the developer on a [stream-aligned](https://teamtopologies.com/key-concepts)
team to implement end to end features as easy as possible.

## Prerequisites

- node lts/gallium: 16.18.0
- npm: 9.1.2
- docker

## Responsibilities

This monorepo is responsible for the following and has the corresponding npm scripts (see package.json):

- Formatting (prettier)
- Commit Management (git-cz)
- Release Management (semantic-release)
- CI/CD within the [DORA framework](https://devops-research.com/assets/state-of-devops-2016.pdf) (gitlab-ci)
