# EMS: Web 🌍

## Overview

### High Level Data Flow

```plantuml
!include https://gitlab.com/-/snippets/2358642/raw/main/style.puml
rectangle web_client {
    rectangle api
    rectangle routes
    rectangle components
    rectangle App.tsx
}
rectangle kubernetes #line.dashed {
    rectangle mosquitto
    rectangle api as backend
    rectangle keycloak
}
backend -- api
keycloak -- api
mosquitto -- api
api -- routes: --rarely calls\n api
api -- components
routes -l- components
routes -- App.tsx
```

## Technologies

- **Build Tool:** Vite
- **UI Framework:** Material UI
- **Global State:** Context API
- **Fetch client:** React Query
- **MQTT Client:** Custom provider/hook
- **Auth:** Keycloak
- **Unit Testing:** Jest and Testing library (Collocated)
- **Integration Testing:** Playwright
- **Error Monitoring:**: Sentry
- **Codegen:** OpenAPI Typescript
- **Form Management:** Formik
- **Schema Validation:** Yup

## Theme

![](https://i.imgur.com/qUIYouN.png)
You'll find all the theme files at `src/theme`. This is self-contained and is based off
of [Minimal UI](https://minimals.cc). When the need for a mobile app arises, a separate repo called `ui-kit` will be
created that will use both this theme folder and react native web. This will be a npm package and mirror the Material UI
component API.

# TODO

## Now

- setup login

## Next

- setup keycloak to exchange creds for token
- playwright integration testing
    - login
    - mocks out network call based on env
