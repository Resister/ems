import { useRoutes } from "react-router-dom";
import { ReactElement } from "react";
import { Button } from "@mui/material";

function Router(): ReactElement | null {
  return useRoutes([
    {
      path: "/",
      element: <Button variant="contained">Home</Button>,
    },
  ]);
}

export default Router;
