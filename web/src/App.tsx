import { Box } from "@mui/material";
import { ThemeProvider } from "@web-craft/component-library";
import { Connector } from "./hooks/mqtt";
import Router from "./routes";

function App(): JSX.Element {
  return (
    <Connector
      options={{
        clientId: `web_${Math.random().toString(36).substring(7)}`,
      }}
      brokerUrl={import.meta.env.VITE_WS_URL}
    >
      <ThemeProvider>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            height: "80vh",
            flex: 1,
          }}
        >
          <Router />
        </Box>
      </ThemeProvider>
    </Connector>
  );
}

export default App;
