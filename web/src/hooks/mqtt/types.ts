import { MqttClient, IClientOptions } from "mqtt";
import * as Buffer from "buffer";
import { ReactNode } from "react";

export interface MqttError {
  name: string;
  message: string;
  stack?: string;
}

export interface ConnectorProps {
  brokerUrl: string;
  options?: IClientOptions;
  parserMethod?: (message: Buffer) => string;
  children: ReactNode;
}

export interface IMqttContext {
  connectionStatus: string | MqttError;
  client?: MqttClient | null;
  parserMethod?: (message: Buffer) => string;
}

export interface IUseSubscription {
  topic: string | string[];
  client?: MqttClient | null;
  message?: string;
  connectionStatus: string | Error;
}
