import { useContext, useEffect, useCallback, useState } from "react";

import { IClientSubscribeOptions } from "mqtt";
// @ts-ignore
import { matches } from "mqtt-pattern";

import MqttContext from "./Context";
import { IMqttContext as Context, IUseSubscription } from "./types";

export default function useSubscription(
  topic: string | string[],
  options: IClientSubscribeOptions = {} as IClientSubscribeOptions
): IUseSubscription {
  const { client, connectionStatus, parserMethod } =
    useContext<Context>(MqttContext);

  const [message, setMessage] = useState("");

  const subscribe = useCallback(async () => {
    client?.subscribe(topic, options);
  }, [client, options, topic]);

  const callback = useCallback(
    (receivedTopic: string, receivedMessage: Buffer) => {
      if ([topic].flat().some((rTopic) => matches(rTopic, receivedTopic))) {
        setMessage(
          parserMethod?.(receivedMessage) || receivedMessage.toString()
        );
      }
    },
    [parserMethod, topic]
  );

  useEffect(() => {
    if (client?.connected) {
      subscribe().then();

      client.on("message", callback);
    }
    return () => {
      client?.removeListener("message", callback);
    };
  }, [callback, client, subscribe]);

  return {
    client,
    topic,
    message,
    connectionStatus,
  };
}
