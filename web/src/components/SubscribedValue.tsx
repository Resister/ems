import { Box } from "@mui/material";
import { useSubscription } from "../hooks/mqtt";

interface SubscribedValueProps {
  topic: string;
}
function SubscribedValue({ topic }: SubscribedValueProps): JSX.Element {
  const { message } = useSubscription(topic);
  return <Box>{message}</Box>;
}
export default SubscribedValue;
