# EMS: API 🚀

## Overview

This REST API follows a very simple version of [hexagonal architecture](). By making use of dependency injection it
avoids cycles and coupling. Takes a **code-first** approach to schema interaction. And uses
the [Chicago School](https://www.goodreads.com/book/show/48927138-unit-testing) style of unit and integration testing.

### High Level Data Flow

```plantuml
!include https://gitlab.com/-/snippets/2358642/raw/main/style.puml
collections web_clients
rectangle domain
rectangle models
database identity_provider
rectangle rest_interface
rectangle clients #line.dashed {
    database document_db
    database timeseries_db
}
web_clients -right- identity_provider
identity_provider -left- rest_interface
web_clients -down- rest_interface
rest_interface -down- domain
domain -down- timeseries_db
domain -down- document_db
domain -right- models
```

From the bottom up, the primary use case is for the user to query for a range of measurements or event data. The time series db
client will capture measurements and sensor site related events. While the document db client will store metadata about
the controllers for each site. Data will continuously be added to the time series db. While document db data will be
seeded via customer's spreadsheet. Next the domain (biz logic) will create whatever data transformations needed to
empower our customers. The domain will only have one dependency and that is the models. This is the root of the graph and
the source of truth schema for the data in the api. From here the open api json schema will be generated. Since, nothing
can depend on the domain, except the models, the clients will be injected at the rest service. The interaction with
identity provider follows [Bearer Authentication](https://swagger.io/docs/specification/authentication/bearer-authentication/) (e.g. no scopes).

> **Important:** The goal here is to be able to easily swap out the time series db for another or the document db for a
> relational one. Similarly, the goal is to easily connect a [tRPC](https://trpc.io) or GraphQL interface in addition (or replacement) to the rest api.

## Technologies

- **Web Framework:** Express
- **Unit Testing:** Jest (co-located)
- **Integration Testing:** Supertest and Nock
- **Logging:** Winston
- **Error Monitoring:**: Sentry
- **Public Interface:** Open API (so clients can generate types)
- **Schema Validation:** Zod

## Running Locally

### Third-Party Services

You'll find a `docker-compose.yaml` in the root. This is only for local development. It will spin up the identity provider and databases. The image versions listed in the compose file are the source of truth and will be reference by those working on the platform. The user for any service will default to your posix `$USER`.

There is also a `dummy-secrets.env` file. This will also be the source of truth of all currently used secret keys (not values). Those working on the platform will reference this and provide the actual values.

To source these dummy secrets and run the services:

```bash
export $(grep -v '^#' dummy-secrets.env | xargs)
cd services
docker compose --env-file=../dummy-secrets.env up
```

### Environment Variables

Environment Variables come in two flavors. Configuration and secrets. These should not be conflated.

#### Configuration

- `E2E`:
  - used to toggle end-to-end testing. Will only work when all the services are running.
- `ENV`:
  - Used when `cfg.yaml` is loaded to determine which configuration block to use. It defaults to `local` if none is provided.

#### Secrets

- `GITLAB_TOKEN`:
  - This is a personal access token for the gitlab api. It is used to lint your gitlab-ci.yml file. We recommend this be added to `.bashrc` or `.zshrc`.
    > **Note:** The kubernetes native secrets api is used in production. These secrets will be added to environment variables at runtime. They should match the keys in `dummy-secrets.env`.

### Running the tests

Once the:

1. Prereqs are installed
2. The variables are available in your environment
3. The docker services are running
4. Node_modules installed

You can run the tests with `npm test`. This will run the unit tests and integration tests. The unit tests are co-located with the code they are testing. The integration tests are in the `tests` directory.
