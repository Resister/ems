import { loadConfig, setupLogger } from "../utils";
import app from "./app";

async function main(): Promise<void> {
  const config = await loadConfig();
  app.use((req, _, next) => {
    req.config = config;
    next();
  });
  const logger = setupLogger(config.logLevel);
  logger.info(`Running with Config: ${JSON.stringify(config)}`);
  app.listen(config.port, config.host);
}

main().then();
