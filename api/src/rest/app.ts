import express from "express";

const app = express();

app.get("/searches", async (_, res) => {
  res.send([]);
});

export default app;
