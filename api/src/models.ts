export default interface Search {
  _id: string;
  [key: string]: unknown;
}
