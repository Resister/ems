import { InfluxDB, Point } from "@influxdata/influxdb-client";

const writeApi = new InfluxDB({
  url: "http://localhost:8086",
  token: "my-super-secret-auth-token",
}).getWriteApi("my-org", "my-bucket", "ns");

const point1 = new Point("temperature")
  .tag("example", "write.ts")
  .floatField("value", 20 + Math.round(100 * Math.random()) / 10);
writeApi.writePoint(point1);

writeApi.close().then();
