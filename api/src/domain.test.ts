import Searches from "./domain";

test("listSearches", () => {
  const searches = new Searches().list();
  expect(searches).toEqual([{ _id: "1", some: "blob" }]);
});
