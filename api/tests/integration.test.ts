import supertest, { SuperTest, Test } from "supertest";
import nock, { Body } from "nock";
import { deepStrictEqual as assertEqual } from "assert";
import app from "../src/rest/app";

function init(
  url?: string,
  path?: string,
  body?: Body,
  method: "get" | "post" | "put" | "delete" = "get",
  responseCode = 200
): SuperTest<Test> {
  if (!process.env.E2E && url && path) {
    nock(url)[method](path).reply(responseCode, body);
  }
  return supertest(app);
}

test("sign in", async () => {
  const client = init();
  const res = await client.get("/searches");
  assertEqual(res.header.authorization.length, 1305);
});
