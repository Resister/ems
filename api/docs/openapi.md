<!-- Generator: Widdershins v4.0.1 -->

<h1 id="sample-api">Sample API v1.0.0</h1>

> Scroll down for code samples, example requests and responses. Select a language for code samples from the tabs above or the mobile navigation menu.

A sample API to illustrate OpenAPI concepts

<h1 id="sample-api-default">Default</h1>

## get\_\_list

> Code samples

```shell
# You can also use wget
curl -X GET /list

```

`GET /list`

Returns a list of stuff

<h3 id="get__list-responses">Responses</h3>

| Status | Meaning                                                 | Description         | Schema |
| ------ | ------------------------------------------------------- | ------------------- | ------ |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | Successful response | None   |

<aside class="success">
This operation does not require authentication
</aside>
